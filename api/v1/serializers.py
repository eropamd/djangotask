from rest_framework import serializers
from django.contrib.auth.models import User
from project.models import Project
from task.models import Task
from timer.models import Timer


class ProjectSerializer(serializers.ModelSerializer):
    class Meta:
        model = Project
        fields = ('id', 'name', 'description', 'start_date', 'end_date','users')


class TaskSerializer(serializers.ModelSerializer):
    class Meta:
        model = Task
        fields = ('id', 'name', 'description', 'project', 'deadline','status','users')


class TimerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Timer
        fields = ('id', 'user', 'task', 'start_time', 'end_time','duration')


class AuthUserSerializer(serializers.Serializer):
    username = serializers.CharField(max_length=255)
    password = serializers.CharField(write_only=True)

    def create(self, validated_data):
        username = validated_data['username']
        password = validated_data['password']
        user = User.objects.create_user(username=username, password=password)
        return user


class UserSerializer(serializers.Serializer):
    class Meta:
        model = User
        fields = ('id', 'username','email')
