from rest_framework.routers import DefaultRouter
from rest_framework import permissions
from drf_yasg.views import get_schema_view
from drf_yasg import openapi
from django.urls import path
from api.v1.views import (ProjectViewSet, TaskViewSet, TimerViewSet, UserViewSet,
                          AuthUserLogoutViewSet, UserUpdateView)

schema_view = get_schema_view(
    openapi.Info(
        title="Task API",
        default_version='v1',
        description="Task API description",
    ),
    public=True,
    permission_classes=(permissions.AllowAny,),
)

router = DefaultRouter()
router.register(r'projects', ProjectViewSet, basename='project')
router.register(r'tasks', TaskViewSet, basename='task')
router.register(r'time-tracker', TimerViewSet, basename='task')
router.register(r'auth/register', UserViewSet, basename='register')
router.register(r'auth/login', UserViewSet, basename='login')
router.register(r'auth/logout', AuthUserLogoutViewSet, basename='logout')
#router.register(r'auth/user', AuthUserViewSet, basename='user')
#router.register(r'auth/user', UserUpdateView, basename='userupdate')

urlpatterns = router.urls

urlpatterns += [
    path('auth/user/', UserUpdateView.as_view(), name='user-update'),
    path('swagger/', schema_view.with_ui('swagger', cache_timeout=0), name='schema-swagger-ui'),
]
