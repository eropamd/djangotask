from rest_framework import viewsets
from rest_framework import status
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
from django.contrib.auth.models import User
from rest_framework_simplejwt.tokens import RefreshToken
from rest_framework.viewsets import GenericViewSet
from rest_framework.mixins import CreateModelMixin
from api.v1.serializers import (ProjectSerializer, TaskSerializer, TimerSerializer,
                                UserSerializer, AuthUserSerializer)
from project.models import Project
from task.models import Task
from timer.models import Timer


class ProjectViewSet(viewsets.ModelViewSet):
    permission_classes = [IsAuthenticated,]
    serializer_class = ProjectSerializer
    queryset = Project.objects.all().prefetch_related("users")


class TaskViewSet(viewsets.ModelViewSet):
    permission_classes = [IsAuthenticated]
    serializer_class = TaskSerializer
    queryset = Task.objects.all().prefetch_related("users")


class TimerViewSet(viewsets.ModelViewSet):
    permission_classes = [IsAuthenticated]
    serializer_class = TimerSerializer
    queryset = Timer.objects.all().prefetch_related("users")


class UserViewSet(GenericViewSet, CreateModelMixin):
    queryset = User.objects.all()
    serializer_class = UserSerializer

    def create(self, request, *args, **kwargs):
        # не очень хорошо
        if 'register' in request.path_info:
            return self.register(request, *args, **kwargs)
        elif 'login' in request.path_info:
            return self.login(request, *args, **kwargs)

    def register(self, request, *args, **kwargs):
        serializer = AuthUserSerializer(data=request.data)

        if serializer.is_valid():
            username = serializer.validated_data['username']
            password = serializer.validated_data['password']

            if self.queryset.filter(username=username).exists():
                return Response({'detail': 'User with this username already exists'},
                                status=status.HTTP_400_BAD_REQUEST)

            user = User.objects.create_user(username=username, password=password)
            refresh = RefreshToken.for_user(user)

            return Response({
                'access_token': str(refresh.access_token),
                'refresh_token': str(refresh)
            },
                status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def login(self, request, *args, **kwargs):
        serializer = AuthUserSerializer(data=request.data)
        if serializer.is_valid():
            username = serializer.validated_data['username']
            password = serializer.validated_data['password']

            user = User.objects.filter(username=username).first()
            if user is not None and user.check_password(password):
                refresh = RefreshToken.for_user(user)
                return Response({'access_token': str(refresh.access_token),
                                 'refresh_token': str(refresh)}, status=status.HTTP_200_OK)
            else:
                return Response({'detail': 'Invalid credentials'},
                                status=status.HTTP_401_UNAUTHORIZED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class AuthUserLogoutViewSet(GenericViewSet):
    permission_classes = [IsAuthenticated]
    queryset = User.objects.all()
    serializer_class = UserSerializer

    def list(self, request, *args, **kwargs):
        refresh_token = request.data.get('refresh_token')
        if refresh_token:
            try:
                token = RefreshToken(refresh_token)
                token.blacklist()
                return Response({'detail': 'Successfully logged out.'},
                                status=status.HTTP_200_OK)
            except Exception as e:
                return Response({'detail': 'Invalid refresh token.'},
                                status=status.HTTP_400_BAD_REQUEST)
        else:
            return Response({'detail': 'Refresh token is required for logout.'},
                            status=status.HTTP_400_BAD_REQUEST)


class UserUpdateView(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, request, *args, **kwargs):
        user_data = {
            'username': self.request.user.username,
            'email': self.request.user.email,
        }
        return Response(user_data, status=status.HTTP_200_OK)

    def post(self, request, *args, **kwargs):
        user = request.user
        new_username = request.data.get("username")
        new_email = request.data.get("email")
        user.username = new_username if new_username else user.username
        user.email = new_email if new_email else user.email
        user.save()
        return Response({"user.id": user.id, "user.username": user.username, "user.email": user.email})
