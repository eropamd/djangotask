from django.contrib.auth.models import User
from rest_framework.test import APITestCase
from rest_framework import status
from rest_framework_simplejwt.tokens import RefreshToken

class ApiTests(APITestCase):

    def setUp(self):
        self.user = User.objects.create_user(username='test13', password='123123123')

    def obtain_tokens(self):

        refresh = RefreshToken.for_user(self.user)
        access_token = str(refresh.access_token)
        refresh_token = str(refresh)
        return access_token, refresh_token

    def test_get_user_view(self):
        access_token, refresh_token = self.obtain_tokens()
        self.client.credentials(HTTP_AUTHORIZATION=f'Bearer {access_token}')
        response = self.client.get('/api/auth/user/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_change_user_name_view(self):

        access_token, refresh_token = self.obtain_tokens()
        self.client.credentials(HTTP_AUTHORIZATION=f'Bearer {access_token}')
        response = self.client.post('/api/auth/user/', {'username': 'new_testuser'})
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['user.username'], 'new_testuser')

    def test_project_view(self):

        access_token, refresh_token = self.obtain_tokens()
        self.client.credentials(HTTP_AUTHORIZATION=f'Bearer {access_token}')
        response = self.client.get('/api/projects/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_tasks_view(self):

        access_token, refresh_token = self.obtain_tokens()
        self.client.credentials(HTTP_AUTHORIZATION=f'Bearer {access_token}')
        response = self.client.get('/api/tasks/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_time_tracker_view(self):

        access_token, refresh_token = self.obtain_tokens()
        self.client.credentials(HTTP_AUTHORIZATION=f'Bearer {access_token}')
        response = self.client.get('/api/time-tracker/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_register_view(self):

        response = self.client.post('/api/auth/register/', {'username': 'newuser9', 'password': '198501ajzRzS'})
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
