from django.contrib import admin
from django.urls import path, include


urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('users.urls')),
    path('api/', include('api.v1.urls')),
    path('', include('project.urls')),
    path('', include('task.urls')),
    path('', include('timer.urls')),
]
