from django.urls import path
from .views import ProjectCreateView, ProjectTaskView, ProjectDeleteView, ProjectEditView


urlpatterns = [
    path('project_new/', ProjectCreateView.as_view(), name='project-new'),
    path('projects/<int:pk>/', ProjectTaskView.as_view(), name='project-task'),
    path('projects/<int:pk>/delete/', ProjectDeleteView.as_view(), name='project-delete'),
    path('projects/<int:pk>/edit/', ProjectEditView.as_view(), name='project-edit'),
]
