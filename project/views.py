from django.shortcuts import get_object_or_404, render, redirect
from django.contrib.auth.decorators import login_required
from django.views import View
from django.contrib.auth.models import User
from django.utils.decorators import method_decorator
from rest_framework.response import Response
from rest_framework import status
from project.models import Project
from timer.models import Timer
from task.models import Task
from api.v1.serializers import ProjectSerializer


class ProjectCreateView(View):
    users = User.objects.all()

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)

    def get(self, request):
        return render(request, 'project_new.html', {'usersquery': self.users})

    def post(self, request):
        serializer = ProjectSerializer(data=request.POST)
        if serializer.is_valid():
            serializer.save()
            return redirect('project-task', pk=serializer.data['id'])
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class ProjectTaskView(View):
    users = User.objects.all()

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)

    def get(self, request, pk):
        user = request.user
        project_data = get_object_or_404(Project, id=pk)
        tasks_for_project = Task.objects.filter(project_id=pk)
        task_statuses = {}
        timer_is_running = False
        for task in tasks_for_project:
            timer_is_running = Timer.objects.filter(task=task, user=user, end_time__isnull=True).exists()
            task_statuses[task.id] = {'timer_is_running': timer_is_running}
        return render(request, 'projects.html', {'project_data': project_data,
                                                 'task_statuses': task_statuses,
                                                 'timer_is_running': timer_is_running,
                                                 'tasks_for_project': tasks_for_project})


class ProjectDeleteView(View):

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)

    def post(self, request, pk):
        obj = get_object_or_404(Project, pk=pk)
        obj.delete()
        return redirect('authenticated_home')


class ProjectEditView(View):

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)

    def get(self, request, pk):
        project_data = get_object_or_404(Project, id=pk)
        users_query = User.objects.all()
        users_in_project = project_data.users.all()
        return render(request, 'project_edit.html', {'project_data': project_data,
                                                     'users_in_project': users_in_project,
                                                     'usersquery': users_query})

    def post(self, request, pk):
        project_instance = get_object_or_404(Project, pk=pk)
        serializer = ProjectSerializer(project_instance, data=request.POST)
        if serializer.is_valid():
            serializer.save()
            return redirect('project-task', pk=pk)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
