from django.db import models
from django.contrib.auth.models import User
from project.models import Project


class Task(models.Model):

    STATUS_CHOICES = [
        ('in_process', 'In Process'),
        ('completed', 'Completed'),
        ('postponed', 'Postponed'),
    ]

    project = models.ForeignKey(Project, on_delete=models.CASCADE)
    name = models.CharField(max_length=255)
    description = models.TextField(blank=True, null=True)
    deadline = models.DateTimeField(blank=True, null=True)
    status = models.CharField(max_length=20, choices=STATUS_CHOICES, default='in_process')
    users = models.ManyToManyField(User)

    def __str__(self):
        return self.project.name+" / "+self.name
