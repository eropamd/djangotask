from django.urls import path
from .views import TaskNewView, TaskDeleteView, TaskEditView


urlpatterns = [
    path('projects/<int:pk>/tasknew/', TaskNewView.as_view(), name='task-new'),
    path('task/<int:pk>/delete/', TaskDeleteView.as_view(), name='task-delete'),
    path('task/<int:pk>/edit/', TaskEditView.as_view(), name='task-edit'),
]
