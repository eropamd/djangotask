from django.shortcuts import get_object_or_404, render, redirect
from django.contrib.auth.decorators import login_required
from django.views import View
from django.contrib.auth.models import User
from django.utils.decorators import method_decorator
from rest_framework.response import Response
from rest_framework import status
from project.models import Project
from timer.models import Timer
from task.models import Task
from api.v1.serializers import TaskSerializer


class TaskNewView(View):
    users = User.objects.all()

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)

    def get(self, request,pk):
        user = request.user
        users_query = User.objects.all()
        project_data = get_object_or_404(Project, id=pk)
        return render(request, 'task_new.html',
                      {'user': user, 'project_data': project_data, 'usersquery': users_query})

    def post(self, request, pk):
        serializer = TaskSerializer(data=request.POST)
        if serializer.is_valid():
            serializer.save()
            return redirect('project-task', pk=pk)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class TaskDeleteView(View):

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)

    def post(self, request, pk):
        obj = get_object_or_404(Task, pk=pk)
        obj.delete()
        return redirect('authenticated_home')


class TaskEditView(View):

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)

    def get(self, request, pk):

        user = request.user
        task_edit = get_object_or_404(Task, id=pk)
        users_in_task = task_edit.users.all()
        task_entries = Timer.objects.filter(task_id=pk)
        users_query = User.objects.all()
        return render(request, 'task_edit.html', {'user': user,
                                                  'users_in_task': users_in_task,
                                                  'task_entries': task_entries,
                                                  'task_edit': task_edit,
                                                  'usersquery': users_query})

    def post(self, request, pk):
        task_data = get_object_or_404(Task, pk=pk)
        serializer = TaskSerializer(task_data, data=request.POST)
        if serializer.is_valid():
            serializer.save()
            return redirect('task-edit', pk=pk)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
