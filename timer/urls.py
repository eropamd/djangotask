from django.urls import path
from .views import TimerStartView, TimerNewtView, TimerEditView


urlpatterns = [
    path('timer/<int:pk>/start/', TimerStartView.as_view(), name='timer_start'),
    path('timer/<int:pk>/new/', TimerNewtView.as_view(), name='timer_new'),
    path('timer/<int:pk>/edit/', TimerEditView.as_view(), name='timer_edit'),
]
