from django.shortcuts import get_object_or_404, render, redirect
from django.contrib.auth.decorators import login_required
from django.views import View
from django.contrib.auth.models import User
from django.utils.decorators import method_decorator
from django.utils import timezone
from timer.models import Timer
from task.models import Task
from datetime import timedelta


class TimerStartView(View):
    users = User.objects.all()

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)

    def get(self, request, pk):
        task = get_object_or_404(Task, id=pk)
        user = request.user
        if not Timer.objects.filter(task=task, user=user, end_time__isnull=True).exists():
            Timer.objects.create(task=task, user=user)
        else:
            time_entry = Timer.objects.filter(task=task, user=user, end_time__isnull=True).first()
            time_entry.end_time = timezone.now()
            time_entry.duration = time_entry.end_time - time_entry.start_time
            time_entry.save()
        return redirect('/projects/' + str(task.project_id) + '/')


class TimerNewtView(View):
    users = User.objects.all()

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)

    def post(self, request, pk):
        data_task = get_object_or_404(Task, id=pk)
        user = request.user
        data_post = request.POST
        new_entry = Timer.objects.create(
            user=user,
            task=data_task,
            start_time=timezone.now(),
            end_time=timezone.now() + timezone.timedelta(hours=int(data_post['hour']), minutes=int(data_post['minutes'])),
        )
        new_entry.duration = new_entry.end_time - new_entry.start_time
        new_entry.save()
        return redirect('/task/' + str(data_task.id) + '/edit/')


class TimerEditView(View):
    users = User.objects.all()

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)

    def post(self, request, pk):
        # cemal case  в питоне не используется
        time_task = get_object_or_404(Timer, id=pk)
        if request.method == 'POST':
            data_post = request.POST
            time_end = time_task.start_time + timedelta(hours=int(data_post['hour']),
                                                       minutes=int(data_post['minutes']))
            time_task.end_time = time_end
            time_task.duration = time_task.end_time - time_task.start_time
            time_task.save()
        return redirect('/task/' + str(time_task.task_id) + '/edit/')
