from django.urls import path
from .views import RegistrationView, LoginView, AuthenticatedHomeView, \
    LogoutUserView, ChangePasswordView, ChangePasswordDoneView


urlpatterns = [
    path('register/', RegistrationView.as_view(), name='register'),
    path('login/', LoginView.as_view(), name='login'),
    path('logout/', LogoutUserView.as_view(), name='logout'),
    path('', AuthenticatedHomeView.as_view(), name='authenticated_home'),
    path('change_password/', ChangePasswordView.as_view(), name='change_password'),
    path('change_password_done/', ChangePasswordDoneView.as_view(), name='change_password_done'),
]
