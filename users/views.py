from django.shortcuts import render, redirect
from django.contrib.auth import login
from django.contrib import messages
from django.views import View
from django.contrib.auth.decorators import login_required
from django.contrib.auth.views import LogoutView, PasswordChangeView
from django.utils.decorators import method_decorator
from django.urls import reverse_lazy
from django.contrib.auth.models import User
from .forms import RegistrationForm, LoginForm
from project.models import Project


class RegistrationView(View):
    template_name = 'register.html'

    def get(self, request):
        form = RegistrationForm()
        return render(request, self.template_name, {'form': form})

    def post(self, request):
        form = RegistrationForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('login')
        return render(request, self.template_name, {'form': form})


class LoginView(View):
    template_name = 'login.html'

    def get(self, request):
        form = LoginForm()
        return render(request, self.template_name, {'form': form})

    def post(self, request):
        form = LoginForm(data=request.POST)
        if form.is_valid():
            login(request, form.get_user())
            return redirect('authenticated_home')
        return render(request, self.template_name, {'form': form})


@method_decorator(login_required, name='dispatch')
class AuthenticatedHomeView(View):
    template_name = 'index.html'

    def get(self, request):
        user = request.user
        user_instance = User.objects.get(id=user.id)
        projects_for_user = Project.objects.filter(users=user_instance)
        return render(request, self.template_name, {'projects_for_user': projects_for_user})


class LogoutUserView(LogoutView):
    next_page = 'login'


@method_decorator(login_required, name='dispatch')
class ChangePasswordView(PasswordChangeView):
    template_name = 'change_password.html'
    success_url = reverse_lazy('change_password_done')

    def form_valid(self, form):
        response = super().form_valid(form)
        messages.success(self.request, 'Your password has been changed successfully.')
        return response


class ChangePasswordDoneView(View):
    template_name = 'change_password_done.html'

    def get(self, request):
        return render(request, self.template_name)
